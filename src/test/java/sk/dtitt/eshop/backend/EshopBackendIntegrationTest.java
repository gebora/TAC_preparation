package sk.dtitt.eshop.backend;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.dtitt.eshop.backend.controller.EshopBackendController;
import sk.dtitt.eshop.backend.db.model.Product;
import sk.dtitt.eshop.backend.db.repository.ProductRepository;
import sk.dtitt.eshop.backend.dto.ProductDto;
import sk.dtitt.eshop.backend.dto.ProductFilter;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.startsWithIgnoringCase;

@SpringBootTest(properties = {"spring.jpa.properties.hibernate.default_schema=PUBLIC"})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@RunWith(SpringRunner.class)
public class EshopBackendIntegrationTest {
    @Autowired
    private EshopBackendController eshopBackendController;
    @Autowired
    private ProductRepository productRepository;
    private List<Product> menProducts;
    private List<Product> womenProducts;

    @Before
    public void before() {
        Product coat = new Product().setBrand("coat").setDescription("Men's coat").setType("men");
        Product coat2 = new Product().setBrand("coat2").setDescription("Men's coat").setType("men");
        Product dress = new Product().setBrand("dress").setDescription("Women's dress").setType("women");
        menProducts = Arrays.asList(coat, coat2);
        womenProducts = Arrays.asList(dress);
        productRepository.saveAll(menProducts);
        productRepository.saveAll(womenProducts);
    }

    @Test
    public void typeFilterAll() {
        ProductFilter productFilter = new ProductFilter();
        List<ProductDto> products = eshopBackendController.products(productFilter).getBody();
        assertThat(products, hasSize(menProducts.size() + womenProducts.size()));
    }

    @Test
    public void typeFilterMen() {
        ProductFilter productFilter = new ProductFilter().setType("men");
        List<ProductDto> products = eshopBackendController.products(productFilter).getBody();
        assertThat(products, hasSize(menProducts.size()));
        assertThat(products, everyItem(hasProperty("description", startsWithIgnoringCase("men"))));
    }

    @Test
    public void typeFilterWomen() {
        ProductFilter productFilter = new ProductFilter().setType("women");
        List<ProductDto> products = eshopBackendController.products(productFilter).getBody();
        assertThat(products, hasSize(womenProducts.size()));
        assertThat(products, everyItem(hasProperty("description", startsWithIgnoringCase("women"))));
    }

    @Test
    public void typeFilterNonExistent() {
        ProductFilter productFilter = new ProductFilter().setType("nonexistent");
        List<ProductDto> products = eshopBackendController.products(productFilter).getBody();
        assertThat(products, hasSize(0));
    }

    @After
    public void after() {
        productRepository.deleteAll(menProducts);
        productRepository.deleteAll(womenProducts);
    }
}