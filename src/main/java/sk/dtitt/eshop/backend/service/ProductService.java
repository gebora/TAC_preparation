package sk.dtitt.eshop.backend.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.aspectj.weaver.ast.Not;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sk.dtitt.eshop.backend.config.ApiResponse;
import sk.dtitt.eshop.backend.db.model.Image;
import sk.dtitt.eshop.backend.db.model.Product;
import sk.dtitt.eshop.backend.db.repository.ProductRepository;
import sk.dtitt.eshop.backend.db.repository.ReviewRepository;
import sk.dtitt.eshop.backend.dto.ImageFilter;
import sk.dtitt.eshop.backend.dto.ProductDto;
import sk.dtitt.eshop.backend.dto.ProductFilter;
import sk.dtitt.eshop.backend.exception.NotFoundException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CommonsLog
@Service
@Transactional
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;


    public List<ProductDto> getProducts(ProductFilter productFilter) {
        log.info("Getting products with filter " + productFilter);
        return productRepository.findAll(Example.of(productFilter.toProbe()))
                .stream().map(ProductDto::new).collect(Collectors.toList());
    }


    public boolean isProductPresent(Long id) {
        Optional<Product> product = productRepository.findById(id);
        boolean isPresent = false;
        if (product.isPresent()) {
            isPresent = true;
        }
        return isPresent;
    }

    public void updateProductByID(Long productID, ProductDto productDto) {
        //dont return ApiResponse from service, service should not handle http response with status code
        //validation moved to controller
        var originalProduct = productRepository.findById(productID).orElseThrow(() -> new NotFoundException("Product with id " + productID + " does not exists"));

        log.info("Updating product with id " + productID);
        originalProduct.setBrand(productDto.getBrand());
        originalProduct.setPrice(productDto.getPrice());
        originalProduct.setDescription(productDto.getDescription());
        productRepository.save(originalProduct);
        // var updatedProduct = productRepository.save(originalProduct);
        // return updatedProduct.getId();
    }

    public byte[] getImage(ImageFilter imageFilter) throws IOException {
        log.info("Getting image with filter " + imageFilter);
        Optional<Product> product = productRepository.findOne(Example.of(imageFilter.toProbe()));
        if (product.isPresent()) {
            return product.get().getImage().getBinaryData();
        } else {
            throw new IOException("Image for product ID " + imageFilter.getProductId() + " was not found.");
        }
    }

    public void resetDb() throws IOException {
        log.info("Resetting database");
        productRepository.deleteAll();

        productRepository.save(new Product()
                .setPrice(14.99f)
                .setBrand("HM")
                .setDescription("5-pack cotton T-shirts")
                .setType("kids")
                .setImage(createImageFromFile("kids5shirts.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(5.99f)
                .setBrand("HM")
                .setDescription("2-pack jersey shorts")
                .setType("kids")
                .setImage(createImageFromFile("kids2shorts.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(15.99f)
                .setBrand("HM")
                .setDescription("3-pack cotton pyjamas")
                .setType("kids")
                .setImage(createImageFromFile("kids3pyjamas.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(9.99f)
                .setBrand("HM")
                .setDescription("Sweatshirt")
                .setType("men")
                .setImage(createImageFromFile("mensweathshirt.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(17.99f)
                .setBrand("HM")
                .setDescription("Cotton chino shorts")
                .setType("men")
                .setImage(createImageFromFile("menchinoshorts.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(12.99f)
                .setBrand("HM")
                .setDescription("Chino shorts Slim Fit")
                .setType("men")
                .setImage(createImageFromFile("menchinoshortsslimfit.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(17.99f)
                .setBrand("HM")
                .setDescription("3-pack T-shirts Slim Fit")
                .setType("men")
                .setImage(createImageFromFile("men3shirts.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(24.99f)
                .setBrand("HM")
                .setDescription("Linen trousers Relaxed Fit")
                .setType("men")
                .setImage(createImageFromFile("menlinentrousers.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(24.99f)
                .setBrand("HM")
                .setDescription("Hooded top")
                .setType("men")
                .setImage(createImageFromFile("menhoodedtop.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(12.99f)
                .setBrand("HM")
                .setDescription("Sweatshirt shorts")
                .setType("men")
                .setImage(createImageFromFile("menshorts.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(9.99f)
                .setBrand("HM")
                .setDescription("Polo shirt Slim Fit")
                .setType("men")
                .setImage(createImageFromFile("menpoloshirt.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(3.99f)
                .setBrand("HM")
                .setDescription("V-neck T-shirt")
                .setType("women")
                .setImage(createImageFromFile("womenvneckshirt.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(14.99f)
                .setBrand("HM")
                .setDescription("High-split skirt")
                .setType("women")
                .setImage(createImageFromFile("womenskirt.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(12.99f)
                .setBrand("HM")
                .setDescription("Smocked top")
                .setType("women")
                .setImage(createImageFromFile("womensmockedtop.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(17.99f)
                .setBrand("HM")
                .setDescription("Calf-length dress")
                .setType("women")
                .setImage(createImageFromFile("womendresscalf.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(24.99f)
                .setBrand("HM")
                .setDescription("Double-breasted jacket")
                .setType("women")
                .setImage(createImageFromFile("womenjacket.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(8.99f)
                .setBrand("HM")
                .setDescription("Ribbed body")
                .setType("women")
                .setImage(createImageFromFile("womenribbedbody.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(12.99f)
                .setBrand("HM")
                .setDescription("Puff-sleeved blouse")
                .setType("women")
                .setImage(createImageFromFile("womenblouse.jpg"))
        );
        productRepository.save(new Product()
                .setPrice(19.99f)
                .setBrand("HM")
                .setDescription("Lyocell dress")
                .setType("women")
                .setImage(createImageFromFile("womenlyocelldress.jpg"))
        );
        log.info("Resetting database DONE");
    }

    private Image createImageFromFile(String fileName) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream is = classLoader.getResourceAsStream("images/" + fileName);
        byte[] fileContent = is.readAllBytes();
        return new Image().setBinaryData(fileContent);
    }
}
