package sk.dtitt.eshop.backend.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sk.dtitt.eshop.backend.db.model.Product;
import sk.dtitt.eshop.backend.db.model.Review;
import sk.dtitt.eshop.backend.db.repository.ProductRepository;
import sk.dtitt.eshop.backend.db.repository.ReviewRepository;
import sk.dtitt.eshop.backend.dto.ReviewDto;
import sk.dtitt.eshop.backend.exception.NotFoundException;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CommonsLog
@Service
@Transactional
@RequiredArgsConstructor
public class ReviewService {
    private final ReviewRepository reviewRepository;
    private final ProductRepository productRepository;


    public List<ReviewDto> getAllReviewsById(Long productId) {
        log.info("Getting all reviews for productId " + productId);
        if (!productRepository.existsById(productId)) {
            throw new NotFoundException("Product with id " + productId + "does not exist");
        }
        return reviewRepository.findAllByProductId(productId).stream().map(ReviewDto::new).collect(Collectors.toList());
    }


    public Long saveReview(ReviewDto review/*, Long productId*/) {
        //we dont return response from service
/*        ApiResponse response = new ApiResponse();
        Optional<Product> product = productRepository.findById(productId);
        if (product.isPresent()) {
            if (ValueRange.of(1, 10).isValidIntValue(review.getRatingPoints())) {//presunut do validacie na controller, len savenut
                reviewRepository.save(new Review()
                        .setProduct(product.get())
                        .setName(review.getName())
                        .setRatingPoints(review.getRatingPoints())
                        .setText(review.getText())
                        .setTimeAndDate(new Date()));
                response.setStatus(HttpStatus.CREATED);
                response.setMessage("New Review item was created");
            } else {
                response.setStatus(HttpStatus.FORBIDDEN);
                response.setMessage("Value is out of the Range. Possible range is 1-10");

            }
        } else {
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setMessage("Product was not found.");
        }*/

       // return reviewRepository.save(review);
        Optional<Product> product = productRepository.findById(review.getProductId());
        if (product.isEmpty()) {
            throw new NotFoundException(String.format("Product with productId %s does not exist", review.getProductId()));
        }
        log.info("Saving review: " + review);
        Review savedReview = reviewRepository.save(Review.builder().product(product.get()).name(review.getName()).text(review.getText()).ratingPoints(review.getRatingPoints()).timeAndDate(new Date()).build());
        return savedReview.getId();
    }

    public ReviewDto getReviewById(Long reviewId){
        log.info("Getting review for id " + reviewId);
        return reviewRepository.findById(reviewId).map(ReviewDto::new).orElseThrow(() -> new NotFoundException("Review with id " + reviewId + " does not exist"));
    }
}