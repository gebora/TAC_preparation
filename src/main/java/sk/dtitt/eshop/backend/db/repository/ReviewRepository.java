package sk.dtitt.eshop.backend.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.dtitt.eshop.backend.db.model.Product;
import sk.dtitt.eshop.backend.db.model.Review;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Long> {
    List<Review> findAllByProductId(Long productId);

}