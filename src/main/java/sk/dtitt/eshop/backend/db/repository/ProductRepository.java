package sk.dtitt.eshop.backend.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import sk.dtitt.eshop.backend.db.model.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {



}
