package sk.dtitt.eshop.backend.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import sk.dtitt.eshop.backend.db.model.Product;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
public class ProductDto {
   // @EqualsAndHashCode.Include
   // private Long id;
    private  String description;
    @NotNull
    private  Float price;
    @NotNull
    @NotEmpty
    private  String brand;

    public ProductDto(Product product) {
       // id = product.getId();
        brand = product.getBrand();
        description = product.getDescription();
        price = product.getPrice();
    }

    public ProductDto(){}

    public ProductDto(Long id, String description, Float price,  String brand) {
       // this.id = id;
        this.description = description;
        this.price = price;
        this.brand = brand;
    }

}
