package sk.dtitt.eshop.backend.dto;

import lombok.Data;
import lombok.extern.apachecommons.CommonsLog;
import sk.dtitt.eshop.backend.db.model.Product;
import sk.dtitt.eshop.backend.db.model.Review;

@Data
@CommonsLog
public class ProductFilter {
    private String type;

    public Product toProbe() {
        return new Product().setType(type);
    }

}
