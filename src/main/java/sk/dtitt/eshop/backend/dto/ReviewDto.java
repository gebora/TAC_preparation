package sk.dtitt.eshop.backend.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import sk.dtitt.eshop.backend.controller.validator.NameValidation;
import sk.dtitt.eshop.backend.db.model.Review;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
@ToString
public class ReviewDto  {
   // @EqualsAndHashCode.Include
   // private Long id;
    @NameValidation
    private String name;
    @Max(10)
    @Min(0)
    private Integer ratingPoints;
    private Date timeAndDate;
    private String text;
    private long productId;

    public ReviewDto(Review review) {
      //  id= review.getId();
        name = review.getName();
        ratingPoints = review.getRatingPoints();
        timeAndDate = review.getTimeAndDate();
        text = review.getText();
    }

    public ReviewDto() {
    }

    public ReviewDto(Long id, String name, Integer ratingPoints, Date timeAndDate, String text) {
      //  this.id = id;
        this.name = name;
        this.ratingPoints = ratingPoints;
        this.timeAndDate = timeAndDate;
        this.text = text;
    }
}
