package sk.dtitt.eshop.backend.dto;

import lombok.Data;
import lombok.extern.apachecommons.CommonsLog;
import sk.dtitt.eshop.backend.db.model.Product;

@Data
@CommonsLog
public class ImageFilter {
    private Long productId;

    public Product toProbe() {
        return new Product().setId(productId);
    }
}
