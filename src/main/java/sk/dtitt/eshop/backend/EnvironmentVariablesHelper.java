package sk.dtitt.eshop.backend;

import lombok.extern.apachecommons.CommonsLog;

//this replaces in-build Spring Boot mechanism because it was not working in Azure Kubernetes Service
//https://docs.spring.io/spring-boot/docs/1.5.6.BUILD-SNAPSHOT/reference/html/boot-features-external-config.html
@CommonsLog
public class EnvironmentVariablesHelper {
    private EnvironmentVariablesHelper() {
    }

    public static void convertEnvironmentVariablesToProperties() {
        log.info("Looking for environment variables");
        convertEnvironmentVariableToProperty("DB_CONNECTION_STRING", "spring.datasource.url");
        convertEnvironmentVariableToProperty("DB_SCHEMA", "spring.jpa.properties.hibernate.default_schema");
    }

    private static void convertEnvironmentVariableToProperty(String environmentVariable, String property) {
        String environmentVariableValue = System.getenv(environmentVariable);
        if (environmentVariableValue != null) {
            log.info("Environment variable [" + environmentVariable + "] found: [" + environmentVariableValue + "]");
            System.setProperty(property, environmentVariableValue);
        } else {
            log.error("Environment variable [" + environmentVariable + "] was NOT found.");
        }
    }
}
