package sk.dtitt.eshop.backend;

import lombok.extern.apachecommons.CommonsLog;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@CommonsLog
@SpringBootApplication
public class EshopBackendApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        log.info("Java application started, preparing environment variables");
        System.setProperty("spring.jpa.hibernate.ddl-auto", "update");
        EnvironmentVariablesHelper.convertEnvironmentVariablesToProperties();

        log.info("Starting spring boot application");
        SpringApplication.run(EshopBackendApplication.class, args);


    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(EshopBackendApplication.class);
    }
}
