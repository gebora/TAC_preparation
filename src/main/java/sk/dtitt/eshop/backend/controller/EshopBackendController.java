package sk.dtitt.eshop.backend.controller;

import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import sk.dtitt.eshop.backend.dto.ImageFilter;
import sk.dtitt.eshop.backend.dto.ProductDto;
import sk.dtitt.eshop.backend.dto.ProductFilter;
import sk.dtitt.eshop.backend.dto.ReviewDto;
import sk.dtitt.eshop.backend.exception.NotFoundException;
import sk.dtitt.eshop.backend.service.ProductService;
import sk.dtitt.eshop.backend.service.ReviewService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@CommonsLog
@CrossOrigin
@RestController
@ControllerAdvice
@RequiredArgsConstructor
public class EshopBackendController {
    private final ProductService productService;
    private final ReviewService reviewService;



    @GetMapping("/products")
    public ResponseEntity<List<ProductDto>> products(@ApiParam ProductFilter productFilter) {
        List<ProductDto> products = productService.getProducts(productFilter);

        CacheControl cacheControl = CacheControl.maxAge(10, TimeUnit.SECONDS);
        return ResponseEntity.ok()
                .cacheControl(cacheControl)
                .body(products);
    }


    @GetMapping("/products/{productId}/reviews")///products/{productId}/reviews ... /customers/{customerId}/product/{productId}
    public ResponseEntity<List<ReviewDto>> getAllReviewsPerProduct(@PathVariable long productId) {
        //get all - none in db- 200 OK with empty body

/*        HttpStatus status;
        List<ReviewDto> reviewDtoList = new ArrayList<>();
        if (!productService.isProductPresent(productId)) {
            status = (HttpStatus.NOT_FOUND);
            // maybe  400 bad request would be also OK, but not found is ok
            return ResponseEntity
                    .status(status)
                    .body(reviewDtoList);
        } else {*/

        //    reviewDtoList = reviewService.getAllReviewsById(productId);//getAllReviewsByProductId
       //     status = HttpStatus.OK;
   //     }

        return ResponseEntity.ok(reviewService.getAllReviewsById(productId));
    }

    //moj endpoint
    @GetMapping("/reviews/{review_id}")
    public ResponseEntity<ReviewDto> getReview(@PathVariable long review_id) {
        var reviewDto = reviewService.getReviewById(review_id);
        return ResponseEntity.ok(reviewDto);
    }


    @PostMapping("/reviews") //is not idempotent, becasue we create new review, even when response is same
    public ResponseEntity<String> createNewReview(@Valid @RequestBody ReviewDto newReview, UriComponentsBuilder uriBuilder/*, @PathVariable Long productId*/) {
        //productId should be moved to newReview - we do create only via request body, not path variable
        //we dont return ApiResponse from service - take a look into service
        //added validation, added exception to exception handler
        // no "createReviews" in uri
/*        Idempotent methods (like PUT): The server state will be the same no matter how many times the same request is made.
          Non-idempotent methods (like POST): Making the same request multiple times may have different effects on the server state.*/
        Long id = reviewService.saveReview(newReview);

/*        return ResponseEntity
                .status(response.getStatus())
                .body(response.getMessage());*/
        return ResponseEntity.created(uriBuilder.path("/reviews/{id}").buildAndExpand(id).toUri()).build();
    }


    @PatchMapping("/products/{id}")//was put before but it seems, it is partial update
    public ResponseEntity<?> updateProductById(@Valid @RequestBody ProductDto updatedProduct, @PathVariable long id) {
        // i have removed id form productdto, because there can be problem on create - id should be generated by system not by user

/*        HttpStatus status;
        String message;
        Float updatedPrice = updatedProduct.getPrice();*/

/*        if (productService.isProductPresent(id)) {
            if (updatedProduct.getBrand() == null || updatedPrice == null) {
                status = HttpStatus.FORBIDDEN;
                message = "Error Message!  Price or Brand are null. Product was not updated";
                log.info(message);
            } else if (updatedProduct.getBrand().equals("") || updatedPrice <= 0) {
                status = HttpStatus.FORBIDDEN;
                message = "Error Message! Brand is Empty or Price was not set correctly. Product was not updated";
                log.info(message);
            } else {
                ApiResponse response = productService.updateProductByID(id, updatedProduct);
                status = response.getStatus();
                message = response.getMessage();
                log.info(message);
            }
        } else {
            status = HttpStatus.NOT_FOUND;
            message = "Product NOT found !";
        }*/
        productService.updateProductByID(id, updatedProduct);
        return ResponseEntity.noContent().build();
        // i was lazy to write all implementation, but you can also return updated object in body or as uri reference.
        // In that case you should return 200
    }


    @GetMapping(value = "/images", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> images(@ApiParam(required = true) ImageFilter imageFilter) throws IOException {
        byte[] image = productService.getImage(imageFilter);
        CacheControl cacheControl = CacheControl.maxAge(30, TimeUnit.MINUTES);
        return ResponseEntity.ok()
                .cacheControl(cacheControl)
                .body(image);
    }

    @GetMapping("/resetdb")// i would use post
    public void resetDb() throws IOException {
        productService.resetDb();
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleConflict(Exception e) {
        log.error(e);
        return e.getMessage();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<Object> handleMethodArgumentNotValidException(final MethodArgumentNotValidException ex) {
        log.warn(ex);
        return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<Object> handleNotFoundExceptionException(final NotFoundException ex) {
        log.warn(ex);
        return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

}
