package sk.dtitt.eshop.backend.controller.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
//import jakarta.validation.Constraint;
//import jakarta.validation.Payload;


@Constraint(validatedBy = NameValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface NameValidation {

    String message() default "Name should not contain any number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
