package sk.dtitt.eshop.backend.controller.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameValidator implements ConstraintValidator<NameValidation, String>
{
    public boolean isValid(String name, ConstraintValidatorContext cxt) {

        return !name.matches(".*\\d.*");
    }
}
