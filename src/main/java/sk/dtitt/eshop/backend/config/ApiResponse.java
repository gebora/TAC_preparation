package sk.dtitt.eshop.backend.config;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ApiResponse {
    private String message;
    private HttpStatus status;

}
