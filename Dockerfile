FROM gradle:jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
#RUN gradle sonarqube --no-daemon
RUN gradle build --no-daemon

FROM adoptopenjdk:11-jre-hotspot
EXPOSE 8080
RUN mkdir /app
COPY --from=build /home/gradle/src/build/libs/eshop-backend.jar /app
ENTRYPOINT ["java", "-jar", "/app/eshop-backend.jar"]