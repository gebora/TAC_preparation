#Running application locally
1) You need AdoptOpenJDK 11 to run this project. Download: https://adoptopenjdk.net/?variant=openjdk11&jvmVariant=hotspot

1) Generate GIT credentials in Azure DevOps

1) Clone your branch locally, preferably with some IDE (IntelliJ)

1) In application root directory you need to copy or rename application.properties.template to application.properties
and fill in correct attributes - user name to use correct database schema and database connection string to connect to database;
example:
    ```
    spring.datasource.url=jdbc:sqlserver://db-tac.database.windows.net:1433;database=dbTAC;user=tacadmin;password=secret
    spring.jpa.properties.hibernate.default_schema=user00
    ```     
    In case you try to connect your local instance from home and not the office, there can be issue with accessing db-tac and you have to install your own local db

1) IDE will recognize gradle project, run EshopBackendApplication (Gradle Tasks: application/bootRun)

1) In case you will use IDEA, SonarLint have to be installed as a plugin and also for Lombok functionalities, plugin is needed altogether with APT turned on
(https://www.baeldung.com/lombok-ide#intellij-apt)

1) Springboot will run embedded Tomcat listening on default port 8080

1) API si now accessible at http://localhost:8080/, for example http://localhost:8080/products

1) Swagger UI is accessible at http://localhost:8080/swagger-ui.html

1) API contains "/resetdb" GET method which populates database with sample data  

#Build info
1) Dockerfile: contains two steps - first we use gradle image to build the application,
second step makes final image from AdoptOpenJDK and built application

1) azure-pipelines.yml: intructs Azure DevOps to build the solution using Dockerfile and after that
it publishes deployment configuration files for release pipeline

1) deployment/eshopbackend.yaml: describes how the Kubernetes should be set up - application pods,
a ClusterIP service do group pods under one IP adress and Ingress which serves as reverse proxy

# Backend Container Environment Variables
DB_CONNECTION_STRING, DB_SCHEMA

Example:
* DB_CONNECTION_STRING=jdbc:sqlserver://10.231.180.130\\DBAAS:51904;database=dbTAC;user=tacadmin;password=Start123
* DB_SCHEMA=eshopXX

This schema needs to be created before in the database.

# Sonar
Don't forget to use SonarLint plugin: https://plugins.jetbrains.com/plugin/7973-sonarlint

# Lombok
This project uses lombok to eliminate some of the boilerplate code - this requires installing Lombok plugin to IDE.

# Database schema generation
use `spring.jpa.hibernate.ddl-auto=create` in application.properties during first run of this project to initialized database schema

# Test commits
1